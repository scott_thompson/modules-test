'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _food = require('./food');

Object.defineProperty(exports, 'food', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_food).default;
  }
});

var _Klass = require('./Klass');

Object.defineProperty(exports, 'Klass', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Klass).default;
  }
});

var _Klass2 = require('./Klass2');

Object.defineProperty(exports, 'Klass2', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Klass2).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }