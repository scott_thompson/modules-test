import babel from 'rollup-plugin-babel';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

export default {
    entry: 'src/index.js',
    // dest: 'dist/index.js',
    plugins: [
        nodeResolve({
            main: true,
            module: true,
            browser: true
        }),
        commonjs({
            include: 'node_modules/**',
            sourceMap: false
        }),
        babel({
            babelrc: false,
            exclude: 'node_modules/**',
            presets: [
                [
                    "es2015",
                    {
                        "modules": false
                    }
                ]
            ],
            plugins: [
                "transform-runtime"
            ],
            runtimeHelpers: true
        })
    ],
    // format: 'umd',
    // moduleName: 'modulesTestLib',
    targets: [
        // { dest: 'dist/index.umd.js', format: 'umd' },
        // { dest: 'dist/index.iife.js', format: 'iife' },
        { dest: 'dist/index.cjs.js', format: 'cjs' },
        { dest: 'dist/index.es.js', format: 'es' }
    ]
};
