import {Klass} from 'modules-test-lib';
console.log('[modulesTest]  - ', Klass);

import MyKlass from './MyKlass';
console.log('[modulesTest]  - ', MyKlass);

const klassDiv = document.createElement('div');
klassDiv.textContent = Klass.toString();

const myKlassDiv = document.createElement('div');
myKlassDiv.textContent = MyKlass.toString();

const klassInstance = new Klass();
const klassInstanceDiv = document.createElement('div');
klassInstanceDiv.textContent = klassInstance.sayHello();

const root = document.getElementById('root');
root.appendChild(klassDiv);
root.appendChild(myKlassDiv);
root.appendChild(klassInstanceDiv);
