import babel from 'rollup-plugin-babel';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

export default {
    entry: 'src/main.js',
    dest: 'output/main.js',
    plugins: [
        nodeResolve({
            main: true,
            module: true
        }),
        commonjs({
            include: 'node_modules/**',
            sourceMap: false
        }),
        babel({
            babelrc: true
        })
    ],
    format: 'cjs'
};
